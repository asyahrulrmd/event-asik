from django.db import models

# Create your models here.
class DaftarTamu(models.Model):
    nama = models.CharField(max_length = 50)

    def __str__(self):
        return "{}. {}".format(self.id, self.nama)


class Peserta(models.Model):
    namaPeserta = models.CharField(max_length = 50)
    ikutKegiatan = models.ForeignKey(
        DaftarTamu, 
        on_delete=models.CASCADE, 
        null=True, blank=True
        )

    def __str__(self):
        return "{}. {}".format(self.id, self.namaPeserta)