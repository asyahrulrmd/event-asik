from django.contrib import admin

# Register your models here.
from .models import DaftarTamu, Peserta

admin.site.register(DaftarTamu)
admin.site.register(Peserta)
