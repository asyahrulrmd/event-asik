from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.base, name='base'),
    path('add-event/', views.addevent, name='addevent'),
    path('listevent/', views.listevent, name='listevent'),
    path('participants/<int:idPeserta>/', views.participants, name='participants'),
    path('delete/<int:delete_id>/', views.delete, name='delete'),
]
