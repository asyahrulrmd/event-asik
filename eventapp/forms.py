from django import forms
from .models import DaftarTamu, Peserta

class DaftarTamuForms(forms.ModelForm):
    class Meta:
        model = DaftarTamu
        fields = [
            'nama',
        ]


class PesertaForms(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'namaPeserta',
        ]