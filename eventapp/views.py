from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import DaftarTamu, Peserta
from .forms import DaftarTamuForms, PesertaForms


# Create your views here.
def base(request):
    return render(request, 'base.html')

def addevent(request):
    peserta_add = DaftarTamu.objects.all()
    peserta_form = DaftarTamuForms(request.POST or None)
    error = None

    if request.method == 'POST':
        if peserta_form.is_valid():

            peserta_form.save()

            return HttpResponseRedirect('/listevent')
    
    context = {
        'page-title':'Add Event',
        'peserta_form':peserta_form,
    }

    return render(request, 'addevent.html', context)


def listevent(request):
    events = DaftarTamu.objects.all()
    participants = Peserta.objects.all()

    context = {
        'page_title':'List Event',
        'events':events,
        'page_title':'List Participants',
        'participants':participants,
    }
    return render(request, 'listevent.html', context)


def participants(request, idPeserta):
    participants_add = Peserta.objects.all()
    participants_form = PesertaForms(request.POST or None)
    error = None

    if request.method == 'POST':
        if participants_form.is_valid():
            participants = Peserta(ikutKegiatan = DaftarTamu.objects.get(id=idPeserta), 
                namaPeserta = participants_form.data['namaPeserta'])
                
            participants.save()
            return HttpResponseRedirect('/listevent/')
            # return redirect('base:listevent')
    
    context = {
        'page-title':'Add Participants',
        'participants':participants_form,
    }
    return render(request, 'participants.html', context)

def delete(request,delete_id):
    DaftarTamu.objects.filter(id=delete_id).delete()
   
    return HttpResponseRedirect('/listevent/')