from django.test import TestCase, Client
from django.urls import resolve
from .views import base, addevent, listevent, participants, delete
from .models import DaftarTamu, Peserta
# Create your tests here.

# ===========LIST EVENT========================
class TestListEvent(TestCase):
    def test_url_listevent(self):
        response = Client().get('/listevent/')
        print(response.status_code)
        self.assertEquals(200, response.status_code)

    def test_listevent_template(self):
        response = Client().get('/listevent/')
        self.assertTemplateUsed(response, 'listevent.html')

    def test_listevent_func(self):
        found =  resolve('/listevent/')
        self.assertEqual(found.func, listevent)


# =============ADD EVENT======================
class TestTambahKegiatan(TestCase):
    def test_url_addevent(self):
        response = Client().get('/add-event/')
        self.assertEqual(response.status_code, 200)

    def test_addevent_func(self):
        found = resolve('/add-event/')
        self.assertEqual(found.func, addevent)

    def test_addevent_template(self):
        response = Client().get('/add-event/')
        self.assertTemplateUsed(response, 'addevent.html')

    def test_add_new_event(self):
        event = DaftarTamu(nama = "PPW")
        event.save()
        self.assertEqual(DaftarTamu.objects.all().count(),1)

    def test_url_event_exist(self):
        response = Client().post('/listevent/', data={'nama':'Coding'})
        self.assertEqual(response.status_code, 200)

# ==============ADD PESERTA=======================
class TestPeserta(TestCase) :
    def setUp(self):
        event = Peserta(namaPeserta="halim")
        event.save()
        self.assertEqual(Peserta.objects.all().count(),1)

    def test_add_participants_post(self):
        response = Client().post('/participants/12/', nama={'namaPeserta':'halim'})
        self.assertEqual(response.status_code, 200)

    def test_add_participants_get(self):
        response = self.client.get('/participants/1/')
        self.assertTemplateUsed(response, 'participants.html')
        self.assertEqual(response.status_code, 200)


# ==========DELETE KEGIATAN===========
class TestHapusKegiatan(TestCase):
    def setUp(self):
        acara = DaftarTamu(nama="Coding")
        acara.save()
        nama = Peserta(namaPeserta="Age")
        nama.save()

    def test_delete_url_event_post(self):
        response = Client().post('/delete/1/')          # saat menghapus data
        self.assertEqual(response.status_code, 302)

    def test_delete_url_event_get(self):
        response = self.client.get('/delete/1/')        # saat mengambil data
        self.assertEqual(response.status_code, 302)
